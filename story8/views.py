from django.shortcuts import render
from django.http import HttpResponse
from django.http import JsonResponse
import requests
import json


# Create your views here.
def lending(request):
    response = {}
    return render(request, 'story8.html', response)

def search(request):
    arg = request.GET['q']
    url_tujuan = 'https://www.googleapis.com/books/v1/volumes?q=' + arg
    r = requests.get(url_tujuan)
    data = json.loads(r.content)
    return JsonResponse(data, safe=False)