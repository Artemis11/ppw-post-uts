from django.test import TestCase, Client
from django.urls import reverse, resolve

from .views import home

# Create your tests here.
class Story9Test(TestCase):
    def test_url_story9(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_template_story9(self):
        response = Client().get('/story9/')
        self.assertTemplateUsed(response, 'story9.html')

    def test_views_tanya(self):
        var = resolve('/story9/')
        self.assertEqual(var.func, home)